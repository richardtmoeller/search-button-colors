~[if#newlook.is.new.look.enabled]
<!-- Part of the Search Button Color Change plugin -->

~[if#NOHOTKEYS.~[pref:d63disablehotkeys]=1][else#NOHOTKEYS]
<script src="/scripts/d63_search_button_color/jquery.hotkeys.js"></script>

<script>
//using the hotkeys jquery plugin, add clear all and clear last filter hotkeys functions
jQuery.hotkeys.options.filterInputAcceptingElements = false
jQuery.hotkeys.options.filterContentEditable = false
jQuery.hotkeys.options.filterTextInputs = false

~[if#NOUSERHOTKEYS.~[pref:d63disableuserhotkeys]=1]
	~[if#CLEARALLHOTKEY.~[pref:d63clearallhotkey]#]
		const d63SearchClearAllHotkey = '~[pref:d63clearallhotkey]';
	[else#CLEARALLHOTKEY]
		const d63SearchClearAllHotkey = 'Alt+A';
	[/if#CLEARALLHOTKEY]

	~[if#CLEARLASTHOTKEY.~[pref:d63clearlasthotkey]#]
		const d63SearchClearLastHotkey = '~[pref:d63clearlasthotkey]';
	[else#CLEARLASTHOTKEY]
		const d63SearchClearLastHotkey = 'Alt+L';
	[/if#CLEARLASTHOTKEY]
	
	~[if#EDITLASTHOTKEY.~[pref:d63editlasthotkey]#]
		const d63SearchEditLastHotkey = '~[pref:d63editlasthotkey]';
	[else#EDITLASTHOTKEY]
		const d63SearchEditLastHotkey = 'Alt+U';
	[/if#EDITLASTHOTKEY]	

	~[if#STUDENTSEARCHSWITCH.~[pref:d63switchstudentshotkey]#]
		const d63SwitchStudentsHotkey = '~[pref:d63switchstudentshotkey]';
	[else#STUDENTSEARCHSWITCH]
		const d63SwitchStudentsHotkey = 'Alt+S';
	[/if#STUDENTSEARCHSWITCH]	

	~[if#STAFFSEARCHSWITCH.~[pref:d63switchstaffhotkey]#]
		const d63SwitchStaffHotkey = '~[pref:d63switchstaffhotkey]';
	[else#STAFFSEARCHSWITCH]
		const d63SwitchStaffHotkey = 'Alt+W';
	[/if#STAFFSEARCHSWITCH]	
    
    ~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]
    	~[if#CONTACTSEARCHSWITCH.~[pref:d63switchcontactshotkey]#]
    		const d63SwitchContactsHotkey = '~[pref:d63switchcontactshotkey]';
    	[else#CONTACTSEARCHSWITCH]
    		const d63SwitchContactsHotkey = 'Alt+C';
    	[/if#CONTACTSEARCHSWITCH]
	[/if#NOCONTACTSHOTKEY]

	~[if#MULTISELECTHOTKEY.~[pref:d63multiselecthotkey]#]
		const d63MultiSelectHotkey = '~[pref:d63multiselecthotkey]';
	[else#MULTISELECTHOTKEY]
		const d63MultiSelectHotkey = 'Alt+M';
	[/if#MULTISELECTHOTKEY]	

[else#NOUSERHOTKEYS]
	~[if#CLEARALLUSERHOTKEY.~[displayprefuser:d63clearalluserhotkey]#]
		const d63SearchClearAllHotkey = '~[displayprefuser:d63clearalluserhotkey]';
	[else#CLEARALLUSERHOTKEY]
		~[if#CLEARALLHOTKEY.~[pref:d63clearallhotkey]#]
			const d63SearchClearAllHotkey = '~[pref:d63clearallhotkey]';
		[else#CLEARALLHOTKEY]
			const d63SearchClearAllHotkey = 'Alt+A';
		[/if#CLEARALLHOTKEY]
	[/if#CLEARALLUSERHOTKEY]
	
 	~[if#CLEARLASTUSERHOTKEY.~[displayprefuser:d63clearlastuserhotkey]#]
		const d63SearchClearLastHotkey = '~[displayprefuser:d63clearlastuserhotkey]';
	[else#CLEARLASTUSERHOTKEY]
		~[if#CLEARLASTHOTKEY.~[pref:d63clearlasthotkey]#]
			const d63SearchClearLastHotkey = '~[pref:d63clearlasthotkey]';
		[else#CLEARLASTHOTKEY]
			const d63SearchClearLastHotkey = 'Alt+L';
		[/if#CLEARLASTHOTKEY]
	[/if#CLEARLASTUSERHOTKEY]
	
 	~[if#EDITLASTUSERHOTKEY.~[displayprefuser:d63editlastuserhotkey]#]
		const d63SearchEditLastHotkey = '~[displayprefuser:d63editlastuserhotkey]';
	[else#EDITLASTUSERHOTKEY]
		~[if#EDITLASTHOTKEY.~[pref:d63editlasthotkey]#]
			const d63SearchEditLastHotkey = '~[pref:d63editlasthotkey]';
		[else#EDITLASTHOTKEY]
			const d63SearchEditLastHotkey = 'Alt+U';
		[/if#EDITLASTHOTKEY]
	[/if#EDITLASTUSERHOTKEY]
	
 	~[if#STUDENTSEARCHSWITCHUSERHOTKEY.~[displayprefuser:d63switchstudentsuserhotkey]#]
		const d63SwitchStudentsHotkey = '~[displayprefuser:d63switchstudentsuserhotkey]';
	[else#STUDENTSEARCHSWITCHUSERHOTKEY]
    	~[if#STUDENTSEARCHSWITCH.~[pref:d63switchstudentshotkey]#]
    		const d63SwitchStudentsHotkey = '~[pref:d63switchstudentshotkey]';
    	[else#STUDENTSEARCHSWITCH]
    		const d63SwitchStudentsHotkey = 'Alt+S';
    	[/if#STUDENTSEARCHSWITCH]
	[/if#STUDENTSEARCHSWITCHUSERHOTKEY]
	
 	~[if#STAFFSEARCHSWITCHUSERHOTKEY.~[displayprefuser:d63switchstaffuserhotkey]#]
		const d63SwitchStaffHotkey = '~[displayprefuser:d63switchstaffuserhotkey]';
	[else#STAFFSEARCHSWITCHUSERHOTKEY]
    	~[if#STAFFSEARCHSWITCH.~[pref:d63switchstaffhotkey]#]
    		const d63SwitchStaffHotkey = '~[pref:d63switchstaffhotkey]';
    	[else#STAFFSEARCHSWITCH]
    		const d63SwitchStaffHotkey = 'Alt+W';
    	[/if#STAFFSEARCHSWITCH]	
	[/if#STAFFSEARCHSWITCHUSERHOTKEY]	
    
    ~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]
     	~[if#CONTACTSSEARCHSWITCHUSERHOTKEY.~[displayprefuser:d63switchcontactsuserhotkey]#]
    		const d63SwitchContactsHotkey = '~[displayprefuser:d63switchcontactsuserhotkey]';
    	[else#CONTACTSSEARCHSWITCHUSERHOTKEY]
        	~[if#CONTACTSEARCHSWITCH.~[pref:d63switchcontactshotkey]#]
        		const d63SwitchContactsHotkey = '~[pref:d63switchcontactshotkey]';
        	[else#CONTACTSEARCHSWITCH]
        		const d63SwitchContactsHotkey = 'Alt+C';
        	[/if#CONTACTSEARCHSWITCH]
    	[/if#CONTACTSSEARCHSWITCHUSERHOTKEY]
	[/if#NOCONTACTSHOTKEY]
	
 	~[if#MULTISELECTUSERHOTKEY.~[displayprefuser:d63multiselectuserhotkey]#]
		const d63MultiSelectHotkey = '~[displayprefuser:d63multiselectuserhotkey]';
	[else#MULTISELECTUSERHOTKEY]
    	~[if#MULTISELECTHOTKEY.~[pref:d63multiselecthotkey]#]
    		const d63MultiSelectHotkey = '~[pref:d63multiselecthotkey]';
    	[else#MULTISELECTHOTKEY]
    		const d63MultiSelectHotkey = 'Alt+M';
    	[/if#MULTISELECTHOTKEY]	
	[/if#MULTISELECTUSERHOTKEY]	
	
[/if#NOUSERHOTKEYS]


function d63SearchClearAllFilters() {
    var clearAllContextStudents = $j('pss-studentsearch #search_switch_btn').length;
    if (clearAllContextStudents > 0 ) {
        $j('button#clearAllFiltersstudents').trigger("click");
        $j('input#studentSearchInput').focus();
    }
    var clearAllContextStaff = $j('pss-staffsearch #search_switch_btn').length;
    if (clearAllContextStaff > 0 ) {
        $j('button#clearAllFiltersstaff').trigger("click");
        $j('input#teacherSearchInput').focus();
    }   
    var clearAllContextContacts = $j('pss-contactsearch #search_switch_btn').length;
    if (clearAllContextContacts > 0 ) {
        $j('button#clearAllFilterscontacts').trigger("click");
        $j('input#contactSearchInput').focus();
    }
}
function d63SearchClearLastFilter() {
    var clearLastContextStudents = $j('pss-studentsearch #search_switch_btn').length;
    if (clearLastContextStudents > 0 ) {
	    $j('pss-studentsearch button[id^="filter_"]').last().trigger("click");
        $j('input#studentSearchInput').focus();
    }
    var clearLastContextStaff = $j('pss-staffsearch #search_switch_btn').length;
    if (clearLastContextStaff > 0 ) {
	    $j('pss-staffsearch button[id^="filter_"]').last().trigger("click");
        $j('input#teacherSearchInput').focus();
    }
    var clearLastContextContacts = $j('pss-contactsearch #search_switch_btn').length;
    if (clearLastContextContacts > 0 ) {
	    $j('pss-contactsearch button[id^="filter_"]').last().trigger("click");
        $j('input#contactSearchInput').focus();
    }  
}
function d63SearchEditLastFilter() {
    var editLastContextStudents = $j('pss-studentsearch #search_switch_btn').length;
    if (editLastContextStudents > 0 ) {
        var d63SearchType = $j('pss-studentsearch button[id^="filter_"]').last().text().trim().split(": ")[0];
        var d63SearchValue = $j('pss-studentsearch button[id^="filter_"]').last().text().trim().split(": ")[1];
	    $j('pss-studentsearch button[id^="filter_"]').last().trigger("click");
	    setTimeout(
            function() {
                $j('#dropDownButtonContainer_studentsSearchFields li').each(function(index) {
                    if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.all2]") {
                        $j(this).trigger('click');
                        $j('input#studentSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("studentSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.last_name2]") {
                        $j(this).trigger('click');
                        $j('input#studentSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("studentSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.first_name2]") {
                        $j(this).trigger('click');
                        $j('input#studentSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("studentSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.student_number]") {
                        $j(this).trigger('click');
                        //$j('#dropDownButtonContainer_studentsSearchFields li:contains("Student Number")').trigger('click');
                        var d63SearchComparator = d63SearchValue.split(" ")[0];
                        var d63StudentNumberSearchString = d63SearchValue.split(/ (.*)/s)[1];
                        //console.log(d63SearchComparator);
                        //console.log(d63StudentNumberSearchString);
                        $j('#dropDownButtonContainer_student_numbercomparators li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchComparator) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                        setTimeout(function() {
                            $j('input#student_numberstudents').val( d63StudentNumberSearchString ).focus(); 
                            document.getElementById("student_numberstudents").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                        
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.state_student_number]") {
                        $j(this).trigger('click');
                        $j('input#studentSearchInput').val( d63SearchValue ).focus();
                        //var d63SearchQuery = 'state_studentnumber=' + d63SearchValue;
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.grade_level]") {
                        $j(this).trigger('click');
                        //$j('#dropDownButtonContainer_studentsSearchFields li:contains("Grade Level")').trigger('click');
                        $j('#dropDownButton__grade_levelstudents').trigger('click');
                        d63SearchValue.split(", ").forEach(function (item) {
                            var d63GradeLevelSearched = item;
                            //console.log(d63GradeLevelSearched);
                            $j('#dropDownButtonContainer_grade_levelstudents label').each(function(index) {
                                //console.log(this);
                                //.log($j(this).html());
                                if ($j(this).html() == d63GradeLevelSearched) {
                                    //console.log("matched!");
                                    $j(this).trigger('click');
                                }
                            });
                        });
                        //var d63SearchQuery = 'grade_level in ' + d63SearchValue;
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.gender]") {
                        $j(this).trigger('click');
                        $j('#dropDownButton__genderstudents').trigger('click');
                        $j('#dropDownButtonContainer_genderstudents li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchValue) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                        //var d63SearchQuery = 'gender=' + d63SearchValue.charAt(0);
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.date_of_birth]") {
                        $j(this).trigger('click');
                        var d63SearchComparator = d63SearchValue.split(" ")[0];
                        var d63DOBSearchString = d63SearchValue.split(" ")[1];
                        //console.log(d63SearchComparator);
                        //console.log(d63DOBSearchString);
                        $j('#dropDownButtonContainer_dobcomparators li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchComparator) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                        setTimeout(function() {
                            $j('input#dobstudents').val( d63DOBSearchString ).focus(); 
                            document.getElementById("dobstudents").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);                        
                        //var d63SearchQuery = 'dob' + d63SearchValue;
                    } else {
                        $j('input#studentSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("studentSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    }
                });
            },
        500);
        //$j('input#studentSearchInput').trigger('keypress', {which: 'A'.charCodeAt(0)});
        //const studentSearchInputElement = document.querySelector('input#studentSearchInput');
        //const keypressEvent = new KeyboardEvent('keydown', {key: "e"});
        //studentSearchInputElement.dispatchEvent(keypressEvent);
        //$j('input#studentSearchInput').trigger("ngModelChange"); //tried change, input, keyup, ngModelChange
    }
    var editLastContextStaff = $j('pss-staffsearch #search_switch_btn').length;
    if (editLastContextStaff > 0 ) {
	    //$j('input#staffSearchInput').val( $j('pss-staffsearch button[id^="filter_"]').last().text().trim().split(": ")[1]);
        //$j('input#teacherSearchInput').focus();
        var d63SearchType = $j('pss-staffsearch button[id^="filter_"]').last().text().trim().split(": ")[0];
        var d63SearchValue = $j('pss-staffsearch button[id^="filter_"]').last().text().trim().split(": ")[1];
	    $j('pss-staffsearch button[id^="filter_"]').last().trigger("click");
	    setTimeout(
            function() {
                $j('#dropDownButtonContainer_staffSearchFields li').each(function(index) {
                    if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.all1]") {
                        $j(this).trigger('click');
                        setTimeout(function() { 
                            $j('input#teacherSearchInput').val( d63SearchValue ).focus(); 
                            document.getElementById("teacherSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.last_name1]") {
                        $j(this).trigger('click');
                        setTimeout(function() { 
                            $j('input#teacherSearchInput').val( d63SearchValue ).focus(); 
                            document.getElementById("teacherSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                        //var d63SearchQuery = 'last_name=' + d63SearchValue;
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.first_name1]") {
                        $j(this).trigger('click');
                        //$j('#dropDownButtonContainer_studentsSearchFields li:contains("First Name")').trigger('click');
                        setTimeout(function() { 
                            $j('input#teacherSearchInput').val( d63SearchValue ).focus(); 
                            document.getElementById("teacherSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                        //var d63SearchQuery = 'first_name=' + d63SearchValue;
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.teacher_number]") {
                        $j(this).trigger('click');
                        var d63SearchComparator = d63SearchValue.split(" ")[0];
                        var d63TeacherNumberSearchString = d63SearchValue.split(/ (.*)/s)[1];
                        //console.log(d63SearchComparator);
                        //console.log(d63TeacherNumberSearchString);
                        $j('#dropDownButtonContainer_teacherNumbercomparators li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchComparator) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                        setTimeout(function() { 
                            $j('input#teacherSearchInput').val( d63TeacherNumberSearchString ).focus(); 
                            document.getElementById("teacherSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                        //var d63SearchQuery = 'student_number' + d63SearchValue;
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.gender]") {
                        $j(this).trigger('click');
                        $j('#dropDownButton__genderstaff').trigger('click');
                        $j('#dropDownButtonContainer_genderstaff li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchValue) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                        //var d63SearchQuery = 'gender=' + d63SearchValue.charAt(0);
                    } else if ($j(this).text().trim() == d63SearchType && d63SearchType == "~[text:psx.js.angular.searchbar_component.staff_type]") {
                        $j(this).trigger('click');
                        $j('#dropDownButton__staffStatusstaff').trigger('click');
                        $j('#dropDownButtonContainer_staffStatusstaff li').each(function(index) {
                            //console.log(this);
                            //console.log($j(this).text());
                            if ($j(this).text() == d63SearchValue) {
                                //console.log("matched!");
                                $j(this).trigger('click');
                            }
                        });
                    } else {
                        $j('input#teacherSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("teacherSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    }
                });
            },
        500);
    }
    var editLastContextContacts = $j('pss-contactsearch #search_switch_btn').length;
    if (editLastContextContacts > 0 ) {
	    //$j('input#contactSearchInput').val( $j('pss-contactsearch button[id^="filter_"]').last().text().trim().split(": ")[1]);
        //$j('input#contactSearchInput').focus();
        var d63SearchType = $j('pss-contactsearch button[id^="filter_"]').last().text().trim().split(": ")[0];
        var d63SearchValue = $j('pss-contactsearch button[id^="filter_"]').last().text().trim().split(": ")[1];
	    $j('pss-contactsearch button[id^="filter_"]').last().trigger("click");
	    setTimeout(
            function() {
                $j('#dropDownButtonContainer_contactsSearchFields li').each(function(index) {
                    if ($j(this).text().trim() == d63SearchType) {
                        $j(this).trigger('click');
                        setTimeout(function() { 
                            $j('input#contactSearchInput').val( d63SearchValue ).focus(); 
                            document.getElementById("contactSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                        }, 100);
                    } else {
                        $j('input#contactSearchInput').val( d63SearchValue ).focus();
                        document.getElementById("contactSearchInput").dispatchEvent(new Event('input', { bubbles: true }));
                    }
                });
            },
        500);
    }
}
function d63SwitchSearchStudents() {
    var switchSearchContextStudents = $j('pss-studentsearch #search_switch_btn').length;
    if (switchSearchContextStudents == 0 ) {
        $j('#search_type_students').trigger("click");
         setTimeout(function() { $j('input#studentSearchInput').focus(); }, 100);
    }
}
function d63SwitchSearchStaff() {
    var switchSearchContextStaff = $j('pss-staffsearch #search_switch_btn').length;
    if (switchSearchContextStaff == 0 ) {
        $j('#search_type_staff').trigger("click");
        setTimeout(function() { $j('input#teacherSearchInput').focus(); }, 100);
    }
}
~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]
function d63SwitchSearchContacts() {
    var switchSearchContextContacts = $j('pss-contactsearch #search_switch_btn').length;
    if (switchSearchContextContacts == 0 ) {
        $j('#search_type_contacts').trigger("click");
         setTimeout(function() { $j('input#contactSearchInput').focus(); }, 100);
    }
}
[/if#NOCONTACTSHOTKEY]
~[if#MULTISELECTPLUGIN.plugin.isEnabled.Multi Select]
function d63MultiSelect() {
    var multiSelectContextStudents = $j('pss-studentsearch #search_switch_btn').length;
    if (multiSelectContextStudents > 0 ) {
	    $j('pss-studentsearch a[href="#MultiSelectDialogStudent"]').trigger("click");
        //$j('textarea#multiSelValsStu').focus();
    }
    var multiSelectContextStaff = $j('pss-staffsearch #search_switch_btn').length;
    if (multiSelectContextStaff > 0 ) {
	    $j('pss-staffsearch a[href="#MultiSelectDialogStaff"]').trigger("click");
        //$j('textarea#multiSelValsStaff').focus();
    }
    var multiSelectContextContacts = $j('pss-contactsearch #search_switch_btn').length;
    if (multiSelectContextContacts > 0 ) {
    }  
}
[/if#MULTISELECTPLUGIN]
// apply hot keys to document
$j(function(){
	$j(document).on('keydown', null, d63SearchClearAllHotkey, d63SearchClearAllFilters);
	$j('#clearAllHotKeyDisplay').html(d63SearchClearAllHotkey);
	$j(document).on('keydown', null, d63SearchClearLastHotkey, d63SearchClearLastFilter);
	$j('#clearLastHotKeyDisplay').html(d63SearchClearLastHotkey);
	$j(document).on('keydown', null, d63SearchEditLastHotkey, d63SearchEditLastFilter);
	$j('#editLastHotKeyDisplay').html(d63SearchEditLastHotkey);
	$j(document).on('keydown', null, d63SwitchStudentsHotkey, d63SwitchSearchStudents);
	$j('#switchStudentsHotKeyDisplay').html(d63SwitchStudentsHotkey);
	$j(document).on('keydown', null, d63SwitchStaffHotkey, d63SwitchSearchStaff);
	$j('#switchStaffHotKeyDisplay').html(d63SwitchStaffHotkey);
	~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]
	$j(document).on('keydown', null, d63SwitchContactsHotkey, d63SwitchSearchContacts);
	$j('#switchContactsHotKeyDisplay').html(d63SwitchContactsHotkey);
	[/if#NOCONTACTSHOTKEY]
	~[if#MULTISELECTPLUGIN.plugin.isEnabled.Multi Select]
	$j(document).on('keydown', null, d63MultiSelectHotkey, d63MultiSelect);
	$j('#multiSelectHotKeyDisplay').html(d63MultiSelectHotkey);
	[/if#MULTISELECTPLUGIN]
	//add hotkey dialog box link to page
	$j('#content-main').prepend('<span style="float:right;font-size:x-small;"><a class="dialogDivC" title="Start Page Keyboard Shortcuts" href="#hiddenKeyboardShortcutsDialog">Keyboard Shortcuts</a>&nbsp;</span>');

});
//Improved method found here (answer provided by wOxxOm): https://stackoverflow.com/questions/38881301/observe-mutations-on-a-target-node-that-doesnt-exist-yet/38882022

function d63SearchWaitForAddedNode(params) {
    new MutationObserver(function(mutations) {
        var el = document.getElementById(params.id);
        var el2 = document.getElementById(params.id2);
        var el3 = document.getElementById(params.id3);
        if (el && el2 && el3) {
            this.disconnect();
            params.done(el);
        }
    }).observe(params.parent || document, {
        subtree: !!params.recursive || !params.parent,
        childList: true,
    });
}
d63SearchWaitForAddedNode({
    id: 'add_search_filter_btn_students',
    id2: 'add_search_filter_btn_staff',
    id3: 'add_search_filter_btn_contacts',
    parent: document.querySelector('.container'),
    recursive: false,
    done: function(el) {
        //console.log(el);
		//add titles to button row
		//$j('pss-searchbar').attr('title','Keyboard Shortcuts: \n-Clear All Searches (' + d63SearchClearAllHotkey + ') \n-Clear Last Search (' + d63SearchClearLastHotkey + ') \n-Edit Last Search (' + d63SearchEditLastHotkey + ') \n-Search Students (' + d63SwitchStudentsHotkey + ') \n-Search Staff (' + d63SwitchStaffHotkey + ') ~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]\n-Search Contacts (' + d63SwitchContactsHotkey + ') [/if#NOCONTACTSHOTKEY]~[if#NOUSERHOTKEYS.~[pref:d63disableuserhotkeys]#1]\n\nConfigure personal hotkeys under Manage Profile.[/if#NOUSERHOTKEYS]');
		//$j('#search_switch_btn').attr('title','Keyboard Shortcuts: \n-Clear All Searches (' + d63SearchClearAllHotkey + ') \n-Clear Last Search (' + d63SearchClearLastHotkey + ') \n-Edit Last Search (' + d63SearchEditLastHotkey + ') \n-Search Students (' + d63SwitchStudentsHotkey + ') \n-Search Staff (' + d63SwitchStaffHotkey + ') ~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]\n-Search Contacts (' + d63SwitchContactsHotkey + ') [/if#NOCONTACTSHOTKEY]~[if#NOUSERHOTKEYS.~[pref:d63disableuserhotkeys]#1]\n\nConfigure personal hotkeys under Manage Profile.[/if#NOUSERHOTKEYS]');
	    //insert Edit Last Search buttons
	    //console.log($j('div.filter-buttons').length);
		$j('#add_search_filter_btn_students').before('<button id="d63SearchEditLastFilterButtonStudent" class="button" title="Edit Last Search" onclick="d63SearchEditLastFilter()" style="height:32px;width:32px;padding:5px;margin-right:10px;margin-left:0px;margin-top:0px;margin-bottom:5px;" ><pds-icon style="height:20px;width:20px;fill:#FFFFFF" name="arrow-undo"> </pds-icon></button>');
		$j('#add_search_filter_btn_staff').before('<button id="d63SearchEditLastFilterButtonStaff" class="button" title="Edit Last Search" onclick="d63SearchEditLastFilter()" style="height:32px;width:32px;padding:5px;margin-right:10px;margin-left:0px;margin-top:0px;margin-bottom:5px;" ><pds-icon style="height:20px;width:20px;fill:#FFFFFF" name="arrow-undo"> </pds-icon></button>');
		$j('#add_search_filter_btn_contacts').before('<button id="d63SearchEditLastFilterButtonContacts" class="button" title="Edit Last Search" onclick="d63SearchEditLastFilter()" style="height:32px;width:32px;padding:5px;margin-right:10px;margin-left:0px;margin-top:0px;margin-bottom:5px;" ><pds-icon style="height:20px;width:20px;fill:#FFFFFF" name="arrow-undo"> </pds-icon></button>');
    }
});

</script>

<div id="hiddenKeyboardShortcutsDialog" class="hide" style="width: 375px;">
    <table class="grid">
        <tr>
            <td class="bold" width="60%">Clear All Searches</td>
            <td id="clearAllHotKeyDisplay"></td>
        </tr>
        <tr>
            <td class="bold">Clear Last Search</td>
            <td id="clearLastHotKeyDisplay"></td>
        </tr>        
        <tr>
            <td class="bold">Edit Last Search</td>
            <td id="editLastHotKeyDisplay"></td>
        </tr>
        <tr>
            <td class="bold">Search Students</td>
            <td id="switchStudentsHotKeyDisplay"></td>
        </tr>
        <tr>
            <td class="bold">Search Staff</td>
            <td id="switchStaffHotKeyDisplay"></td>
        </tr>
        ~[if#NOCONTACTSHOTKEY.~[pref:d63disableswitchcontactshotkey]#1]
        <tr>
            <td class="bold">Search Contacts</td>
            <td id="switchContactsHotKeyDisplay"></td>
        </tr>
        [/if#NOCONTACTSHOTKEY]
        ~[if#MULTISELECTPLUGIN.plugin.isEnabled.Multi Select]
        <tr>
            <td class="bold">Open MultiSelect</td>
            <td id="multiSelectHotKeyDisplay"></td>
        </tr>
        [/if#MULTISELECTPLUGIN]
        ~[if#NOUSERHOTKEYS.~[pref:d63disableuserhotkeys]#1]
        <tr>
            <td colspan=100%>Configure personal hotkeys under <a href="/admin/usersettings.html?frn=~(userfrn)">Manage Profile</a></td>
        </tr>
        [/if#NOUSERHOTKEYS]
    </table>
</div>


[/if#NOHOTKEYS]

[else#newlook]
[/if#newlook]