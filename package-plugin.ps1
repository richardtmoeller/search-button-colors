"Reading plugin.xml file"
[XML]$xmlfile = Get-Content .\plugin.xml
$plugin_version = $xmlfile.plugin.[version]
$plugin_name = $xmlfile.plugin.name -replace '[\W]', '<>' -replace '><', '' -replace '<>','_'
$zipFileName = ".\" + $plugin_name + "_v" + $plugin_version + ".zip"

# Check if zip file already exists
If(Test-path $zipFileName) {
	#prompt Y/N for delete existing file>
	Do
	{
		$deleteFile = Read-Host "Packaged Plugin Zip File Already Exists, Delete? (Y/N)"
		switch ($deleteFile)
		{
			'Y' {
				'You chose Yes, the file will be deleted.'
				Remove-item $zipFileName
			} 'N' {
				'You chose No, exiting now.'
				timeout 5
				exit
			}
		}
	} Until ($deleteFile -in 'Y','N')
}

# list of files and folders to potentially include:
$assetFiles = 
".\plugin.xml",
".\web_root",
".\queries_root",
".\user_schema_root",
".\permissions_root"

#iterate through the file list and use 7zip to zip up the file
Foreach ($entry in $assetFiles)
{
  If(Test-path $entry) {
	  C:\"Program Files"\7-Zip\7z.exe a -tzip $zipFileName $entry
  }
}

